# WMS Circ App
An Electron-based App for OCLC's WMS Circulation Module.

If it's an electron app, is it *really* an app? I know that some people will say no, that it has to be a native system application built with Assembly and whispered incantations to Darth Vader. I don't have that kind of bare-metal development experience. What I do have is experience as a web developer, and WMS is a web-based service.

I *could* get into how every application technology is built upon the framework created by another, earlier, application technology, but that would be turtles all the way down.

### WMS, meet Electron.

This app does only what it is supposed to do--check items out to patrons; check patron items back in--and nothing else. Administrative functionality is left to the WMS web interface.

## Ingredients
* Nativefier 
  * [GitHub - jiahaog/nativefier](https://github.com/jiahaog/nativefier)
* Node.js
  * [Node.js](https://nodejs.org/en/)



Since the end result was an app that needed to run on a 32-bit version of Windows, I installed all my tools on a computer running Windows 10. I primarily use a Mac, but it is difficult if not impossible to create a 32-bit Windows app on a modern Mac running MacOS.


## Here's the recipe:
```
nativefier "https://YOUR_WMS_NAME_HERE.share.worldcat.org/wms/cmnd/circ/" --name "Circulation" --app-version 0.1 --platform win32 --disable-context-menu --disable-dev-tools --single-instance --internal-urls ".*?\.*.\.*?" --inject ..\resources\hide.js --icon ..\resources\BelkCirc.png ..\output\
```

[A more readable version](https://github.com/jerrywaller/BelkCirc/blob/master/CommandSyntax.txt)


The line
`--icon ..\resources\BelkCirc.png`
was supposed to replace the generic Electron icon with a custom one I made myself. It kept failing, however, and I could not get it to work.

I highly, *highly* recommend reading the [Nativefier API documentation](https://github.com/jiahaog/nativefier/blob/master/docs/api.md) for understanding the flags used. 

Also, I'm working out how to deal with the issues that shibboleth authentication brings up. By default, Nativefier only allows navigation to the primary url and its secondary(?) domains. The primary URL in this case is, I think, share.worldcat.org 

Elon's shibboleth authentication goes to an Elon address (and perhaps other resources). The Nativefier flag `--internal-urls` in the above code uses a regular expression of unknown flavor to allow the app to access any external url. **This is not ideal, nor a good practice.** I need to determine how to allow elon.edu without a tab opening in the default web browser. This breaks the continutity within the app and pretty much defeats the purpose of having a dedicated app. 
